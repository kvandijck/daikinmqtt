#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <netdb.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <mosquitto.h>

#include "lib/libt.h"
#include "lib/libe.h"
#include "lib/liburi.h"

#define NAME "daikinmqtt"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* generic error logging */
#define LOG_MQTT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_MQTT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_MQTT conflict
#endif
static void mqttlog(int loglevel, const char *str);

static int max_loglevel = LOG_WARNING;
static int logtostderr = -1;

static void set_loglevel(int new_loglevel)
{
	max_loglevel = new_loglevel;
	if (!logtostderr)
		setlogmask(LOG_UPTO(max_loglevel));
}

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel & LOG_PRIMASK, "%s", str);

	} else if ((loglevel & LOG_PRIMASK) <= max_loglevel) {
		struct timespec tv;
		char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}
	if (loglevel & LOG_MQTT) {
		mqttlog(loglevel, str);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": control/monitor daikin airco via MQTT\n"
	"usage:	" NAME " [OPTIONS ...] -h HOSTNAME_OR_IP_OF_DAIKIN\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -m, --mqtt=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -h, --host=DAIKIN	Sepcify daikin host\n"
	" -n, --dryrun		No action\n"

	" -w, --work=TOPIC	TOPIC base for working state\n"
	" -k, --homekit=TYPE	enable additional homekit topics for TYPE\n"
	"			thermostat|heatercooler\n"
	"\n"
	"Topics\n"
	" WORKTOPIC/smode	0|auto|dry|cool|hot|fan\n"
	" WORKTOPIC/stemp	temperature setpoint\n"
	" WORKTOPIC/fan		fan setpoint: 'auto|silent|0..1\n"
	" WORKTOPIC/wing	wing setpoint: 'none|horz|vert|both\n"
	" WORKTOPIC/itemp	room temperature\n"
	" WORKTOPIC/ihum	room relative humidity (0..1)\n"
	" WORKTOPIC/otemp	outside temperature\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "mqtt", required_argument, NULL, 'm', },
	{ "daikin", required_argument, NULL, 'h', },
	{ "dryrun", required_argument, NULL, 'n', },

	{ "work", required_argument, NULL, 'w', },
	{ "homekit", required_argument, NULL, 'k', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?m:h:nw:k:";

/* config */
static const char *work_topic = "daikin";
static int work_topiclen;

static int dryrun;
static int homekit;
#define HKTHERMOSTAT	1
#define HKAIRCO		2
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 0;
static int mqtt_qos = 0;

static const char *daikin_host;

/* state */
static struct mosquitto *mosq;
static struct sockaddr_storage sockaddr;
static int sockaddrlen;
static const char *daikin_ipstr;
static int dirty;
static int mqtt_ready;
static int dev_ready;

static struct setp {
	int pow;
	int mode;
	double temp;
	int fan;
	int wing;
} modecfgs[8] = {
#define NULL_SETP	{ -1, -1, NAN, -1, -1, }
	/* preset to invalid */
	[0] = NULL_SETP,
	[1] = NULL_SETP,
	[2] = NULL_SETP,
	[3] = NULL_SETP,
	[4] = NULL_SETP,
	[5] = NULL_SETP,
	[6] = NULL_SETP,
	[7] = NULL_SETP,
};

/* current values, reflects device state */
static struct setp curr;
/* wanted state */
static struct setp setp = NULL_SETP;
/* mqtt initial state */
static struct setp setp_mqtt = NULL_SETP;

static char *saved_state;
static char *saved_pow;
static char *saved_mode;
static char *saved_hkcurstate;
static char *saved_hksetstate;
static char *saved_stemp;
static char *saved_fan;
static char *saved_wing;
static char *saved_itemp;
static char *saved_otemp;
static char *saved_ihum;

#define xfree(x)	({ if (x) free(x); (x) = NULL; })

static void mqtt_pub(const char *topic, const char *payload, int retain, char **cached);
static void daikin_http_recvd(int fd, void *dat);
static const char *payloadfmt(const char *fmt, ...);
static const char *topicfmt(const char *fmt, ...);

/* signal handler */
static volatile int sigterm;

static void signalrecvd(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		}
	}
}

/* self-sync util */
static char myuuid[128];
static const char selfsynctopic[] = "tmp/selfsync";
static void send_self_sync(struct mosquitto *mosq, int qos)
{
	int ret;

	sprintf(myuuid, "%i-%li-%i", getpid(), (long)time(NULL), rand());

	ret = mosquitto_subscribe(mosq, NULL, selfsynctopic, qos);
	if (ret)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", selfsynctopic, mosquitto_strerror(ret));
	ret = mosquitto_publish(mosq, NULL, selfsynctopic, strlen(myuuid), myuuid, qos, 0);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", selfsynctopic, mosquitto_strerror(ret));
}

static int is_self_sync(const struct mosquitto_message *msg)
{
	return !strcmp(msg->topic, selfsynctopic) &&
		!strcmp(myuuid, msg->payload ?: "");
}

/* usefull tricks */
static char *csprintf_str;
__attribute__((format(printf,1,2)))
static const char *csprintf(const char *fmt, ...)
{
	va_list va;

	xfree(csprintf_str);
	va_start(va, fmt);
	vasprintf(&csprintf_str, fmt, va);
	va_end(va);
	return csprintf_str;
}

static const char *const modes[] = {
	[0] = "auto",
	[1] = "auto",
	[2] = "dry",
	[3] = "cool",
	[4] = "hot",
	[5] = "auto",
	[6] = "fan",
	[7] = "auto",
};
#define NMODES (sizeof(modes)/sizeof(modes[0]))
static const char *const hksetstates[] = {
	[0] = "auto",
	[1] = "heat",
	[2] = "cool",
};
#define NHKSETSTATES (sizeof(hksetstates)/sizeof(hksetstates[0]))
static const char *const hkthrmstates[] = {
	[0] = "off",
	[1] = "heat",
	[2] = "cool",
	[3] = "auto",
};
#define NHKTHRMSTATES (sizeof(hkthrmstates)/sizeof(hkthrmstates[0]))

__attribute__((unused))
static int strtomode(const char *mode)
{
	int j;
	int chr = tolower(*mode);

	for (j = 1; j < NMODES; ++j) {
		if (modes[j][0] == chr)
			return j;
	}
	return -1;
}

__attribute__((unused))
static const char *modetostr(int mode)
{
	if (mode >= 0 && mode < NMODES)
		return modes[mode];
	else
		return "?";
}

__attribute__((unused))
static int modetohkthrmstate(int mode)
{
	static const int table[NMODES] = { 3, 3, 3, 2, 1, 3, 3, 3, };

	if (mode >= 0 && mode < NMODES)
		return table[mode];
	else
		return -1;
}
__attribute__((unused))
static int modetohksetstate(int mode)
{
	static const int table[NMODES] = { 0, 0, 0, 2, 1, 0, 0, 0, };

	if (mode >= 0 && mode < NMODES)
		return table[mode];
	else
		return -1;
}
__attribute__((unused))
static int modetohkcurstate(int mode)
{
	static const int table[NMODES] = { 1, 1, 1, 3, 2, 1, 1, 1, };

	if (mode >= 0 && mode < NMODES)
		return table[mode];
	else
		return -1;
}

__attribute__((unused))
static double strtod_safe(const char *str)
{
	double val;
	char *endp;

	val = strtod(str, &endp);
	if (endp > str)
		return val;
	return NAN;
}

__attribute__((unused))
static const char *dtostr_special(double value, const char *fmt, const char *nanstr)
{
	static char buf[32];

	if (isnan(value))
		return nanstr;
	sprintf(buf, fmt, value);
	return buf;
}

__attribute__((unused))
static int strtofan(const char *str)
{
	double val;

	switch (toupper(*str)) {
	case 'A':
		return 'A';
	case 'S': /* silent */
		return 'B';
	default:
		val = strtod(str, NULL);
		if (val >= 0 && val <= 1)
			return '3' + lround(strtod(str, NULL) *4);
		return 'A';
	}
}
__attribute__((unused))
static const char *fantostr(int chr)
{
	switch (chr) {
	case 'A':
	default:
		return "auto";
	case 'B': /* silent */
		return "silent";
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
		return csprintf("%.2f", (chr - '3')/4.0);
	}
}

__attribute__((unused))
static const char *wingtostr(int val)
{
	static const char *const table[] = {
		[0] = "none",
		[1] = "vert",
		[2] = "horz",
		[3] = "both",
	};
	if (val >= 0 && val < 4)
		return table[val];
	return "x";
}
__attribute__((unused))
static int strtowing(const char *str)
{
	switch (tolower(*str)) {
	case 'n':
	default:
		return 0;
	case 'v':
		return 1;
	case 'h':
		return 2;
	case 'b':
		return 3;
	}
}

__attribute__((unused))
static const char *setptostr(const struct setp *p, char *str)
{
	sprintf(str, "%s %s %.1f° %s %s"
			, p->pow ? "on" : "off"
			, modetostr(p->mode)
			, p->temp
			, fantostr(p->fan)
			, wingtostr(p->wing)
		      );
	return str;
}

/* daikin iface */
int daikin_resolve(const char *hostname)
{
	int ret;
	struct addrinfo *resolved, *h;
	struct uri uri = {};
	void *ntopptr;

	/* resolve */
	struct addrinfo hints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_STREAM,
	};
	ret = getaddrinfo(hostname, "80", &hints, &resolved);
	if (ret)
		mylog(LOG_ERR, "could not resolve host %s: %s", uri.host, gai_strerror(ret));

	if (!resolved)
		mylog(LOG_ERR, "host %s didn't resolve", uri.host);

	else {
		h = resolved;
		switch (h->ai_family) {
		case AF_INET6:
			ntopptr = &((struct sockaddr_in6*)h->ai_addr)->sin6_addr;
			break;
		case AF_INET:
		default:
			ntopptr = &((struct sockaddr_in*)h->ai_addr)->sin_addr;
			break;
		}
		static char ipstr[128];
		daikin_ipstr = inet_ntop(h->ai_family, ntopptr, ipstr, sizeof(ipstr));
		sockaddrlen = h->ai_addrlen;
		memcpy(&sockaddr, h->ai_addr, sockaddrlen);
	}
	freeaddrinfo(resolved);
	return 0;
}

static char rbuf[16*1024];
static char tbuf[1024];
static int rfill;

/* sync initial data */
static void initial_merge(void)
{
	/* compare state */
	if ((setp_mqtt.pow >= 0 && setp_mqtt.pow != curr.pow)
			|| (setp_mqtt.mode >= 0 && setp_mqtt.mode != curr.mode)
			|| (!isnan(setp_mqtt.temp) && fabs(setp_mqtt.temp - curr.temp) > 0.1)
			|| (setp_mqtt.fan >= 0 && setp_mqtt.fan != curr.fan)
			|| (setp_mqtt.wing >= 0 && setp_mqtt.wing != curr.wing)
	   )
		mylog(LOG_WARNING | LOG_MQTT, "initial conflict: %s <-> %s",
				setptostr(&curr, rbuf),
				setptostr(&setp_mqtt, rbuf+256));
	else
		mylog(LOG_NOTICE, "initial data ok");

	/* merge state */
	if (setp.pow < 0)
		setp.pow = curr.pow;
	if (setp.mode < 0)
		setp.mode = curr.mode;
	if (isnan(setp.temp))
		setp.temp = curr.temp;
	if (setp.fan < 0)
		setp.fan = curr.fan;
	if (setp.wing < 0)
		setp.wing = curr.wing;
}

struct request {
	struct request *next;
	int connected;
	char str[];
};

static struct request *reqq, *reqqend;

static int daikin_request_start(struct request *req);
static int daikin_request_send(int sock, struct request *req);
static int daikin_request(const char *str)
{
	struct request *req;
	int len;

	len = strlen(str ?: "");
	req = malloc(sizeof(*req)+len+1);
	if (!req)
		mylog(LOG_ERR, "malloc %li: %s", (long)sizeof(*req)+len+1, ESTR(errno));
	memset(req, 0, sizeof(*req));

	strcpy(req->str, str);
	if (reqqend) {
		/* append */
		reqqend->next = req;
		reqqend = req;
		return 0;
	} else {
		/* set first */
		reqqend = reqq = req;
		/* initiate transfer */
		return daikin_request_start(req);
	}
}

static int daikin_request_start(struct request *req)
{
	int sock;

	/* create socket */
	sock = socket(sockaddr.ss_family, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if (sock < 0)
		mylog(LOG_ERR, "socket: %s", ESTR(errno));

	libe_add_fd(sock, daikin_http_recvd, req);

	if (connect(sock, (void *)&sockaddr, sockaddrlen) < 0) {
		if (errno != EINPROGRESS) {
			close(sock);
			mylog(LOG_ERR, "connect: %s", ESTR(errno));
			exit(1);
			return -1;
		}
		/* wait for connection success */
		libe_mod_fd(sock, LIBE_WR);
	} else {
		req->connected = 1;
		/* send already */
		daikin_request_send(sock, req);
	}
	return sock;
}

static int daikin_request_send(int sock, struct request *req)
{
	int len, ret;

	len = sprintf(tbuf, "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: Keep-Alive\r\n\r\n",
			req->str,
			daikin_ipstr);
	ret = send(sock, tbuf, len, 0);
	if (ret < 0)
		mylog(LOG_ERR, "send %s: %s", daikin_host, ESTR(errno));
	else
		mylog(LOG_DEBUG, "http:> %s", req->str);
	return ret;
}

static void request_info(void *dat)
{
	daikin_request("/common/basic_info");
}
static void request_state(void *dat)
{
	daikin_request("/aircon/get_sensor_info");
	daikin_request("/aircon/get_control_info");
	libt_add_timeout(30, request_state, dat);
}

static void send_update(void *dat)
{
	static char ctl[256];

	if (!mqtt_ready || !dev_ready)
		return;

	sprintf(ctl, "/aircon/set_control_info?pow=%u&mode=%u&stemp=%s&shum=0&f_dir=%u&f_rate=%c",
			setp.pow, setp.mode,
			dtostr_special(setp.temp, "%.1f", "M"),
			setp.wing,
			setp.fan);
	if (dryrun) {
		mylog(LOG_INFO, "http:> %s", ctl);
		dirty = 0;
		return;
	}
	mylog(LOG_DEBUG, "http:> %s", ctl);
	daikin_request(ctl);
	dirty = 0;
	request_state(NULL);
}

static void update(void)
{
	dirty = 1;
	libt_add_timeout(0.2, send_update, NULL);
}

static int hextoint(int chr)
{
	if (chr >= '0' && chr <= '9')
		return 0 + chr - '0';
	else if (chr >= 'a' && chr <= 'f')
		return 10 + chr - 'a';
	else if (chr >= 'A' && chr <= 'F')
		return 10 + chr - 'A';
	else
		return 0;
}
static void http_url_decode(char *str)
{
	char *dst;

	for (dst = str; *str; ) {
		if (*str == '%' && str[1] && str[2]) {
			*dst++ = hextoint(str[1])*16 + hextoint(str[2]);
			str += 3;
		} else
			*dst++ = *str++;
	}
	*dst = 0;
}

static void daikin_offline(void *dat)
{
	mqtt_pub(topicfmt("%s/state", work_topic), "offline", 1, &saved_state);
}

static void daikin_recvd_payload(char *str, int len)
{
	char *tok, *val;
	double fval;
	int idx;

	mylog(LOG_DEBUG, "http:< %s", str);
	for (tok = strtok(str, ","); tok; tok = strtok(NULL, ",")) {
		val = strchr(tok, '=');
		if (val) {
			*val++ = 0;
			http_url_decode(val);
		}
		if (!strcmp("pow", tok)) {
			curr.pow = strtoul(val, NULL, 0);
			mqtt_pub(topicfmt("%s/pow", work_topic), payloadfmt("%i", curr.pow), 1, &saved_pow);
			if (!curr.pow)
				mqtt_pub(topicfmt("%s/state", work_topic), "off", 1, &saved_state);
			if (!curr.pow && homekit)
				mqtt_pub(topicfmt("%s/homekit/curstate", work_topic), "0", 1, &saved_hkcurstate);
			if (mqtt_ready && dev_ready && !dirty)
				setp.pow = curr.pow;

		} else if (!strcmp(tok, "mode")) {
			curr.mode = strtoul(val, NULL, 0) ?: 1;
			mqtt_pub(topicfmt("%s/mode", work_topic), modetostr(curr.mode), 1, &saved_mode);
			if (curr.pow)
				mqtt_pub(topicfmt("%s/state", work_topic), modetostr(curr.mode), 1, &saved_state);
			if (curr.pow && homekit == HKAIRCO)
				mqtt_pub(topicfmt("%s/homekit/curstate", work_topic),
						payloadfmt("%i", modetohkcurstate(curr.mode)), 1, &saved_hkcurstate);
			if (curr.pow && homekit == HKTHERMOSTAT)
				mqtt_pub(topicfmt("%s/homekit/curstate", work_topic),
						payloadfmt("%i", modetohkthrmstate(curr.mode)), 1, &saved_hkcurstate);
			if (homekit == HKAIRCO)
				mqtt_pub(topicfmt("%s/homekit/setstate", work_topic),
						payloadfmt("%i", modetohksetstate(curr.mode)), 1, &saved_hksetstate);
			if (homekit == HKTHERMOSTAT)
				mqtt_pub(topicfmt("%s/homekit/setstate", work_topic),
						payloadfmt("%i", modetohkthrmstate(curr.mode)), 1, &saved_hksetstate);
			libt_add_timeout(120, daikin_offline, NULL);
			if (mqtt_ready && dev_ready && !dirty)
				setp.mode = curr.mode;

		} else if (!strcmp(tok, "stemp")) {
			curr.temp = strtod_safe(val);
			mqtt_pub(topicfmt("%s/stemp", work_topic), payloadfmt("%.1f", curr.temp), 1, &saved_stemp);
			if (mqtt_ready && dev_ready && !dirty)
				setp.temp = curr.temp;

		} else if (!strcmp(tok, "f_rate")) {
			curr.fan = *val;
			mqtt_pub(topicfmt("%s/fan", work_topic), fantostr(curr.fan), 1, &saved_fan);
			if (mqtt_ready && dev_ready && !dirty)
				setp.fan = curr.fan;

		} else if (!strcmp(tok, "f_dir")) {
			curr.wing = strtoul(val ?: "", NULL, 0);
			mqtt_pub(topicfmt("%s/wing", work_topic), wingtostr(curr.wing), 1, &saved_wing);
			if (mqtt_ready && dev_ready && !dirty)
				setp.wing = curr.wing;

		} else if (!strcmp(tok, "htemp")) {
			fval = strtod_safe(val);
			mqtt_pub(topicfmt("%s/itemp", work_topic), payloadfmt("%.1f", fval), 1, &saved_itemp);

		} else if (!strcmp(tok, "hhum")) {
			fval = strtod_safe(val);
			mqtt_pub(topicfmt("%s/ihum", work_topic), payloadfmt("%.2f", fval/100), 1, &saved_ihum);

		} else if (!strcmp(tok, "otemp")) {
			fval = strtod_safe(val);
			mqtt_pub(topicfmt("%s/otemp", work_topic), payloadfmt("%.1f", fval), 1, &saved_otemp);

		} else if (!strcmp(tok, "name")
				|| !strcmp(tok, "rev")
				|| !strcmp(tok, "ver")
				|| !strcmp(tok, "id")) {
			/* log some properties in syslog */
			mylog(LOG_NOTICE, "%s: %s", tok, val);

		} else if (!strncmp(tok, "dt", 2)) {
			idx = strtol(tok+2, NULL, 0);
			if (idx == 1) {
				int j;
				for (j = 0; j < sizeof(modecfgs)/sizeof(modecfgs[0]); ++j)
					modecfgs[j].temp = NAN;
			}
			modecfgs[idx].temp = strtod_safe(val);
		} else if (!strncmp(tok, "dfr", 3)) {
			idx = strtol(tok+3, NULL, 0);
			modecfgs[idx].fan = *val;
		} else if (!strncmp(tok, "dfd", 3)) {
			idx = strtol(tok+3, NULL, 0);
			modecfgs[idx].wing = *val;
		}
	}
}

static void daikin_http_recvd(int fd, void *dat)
{
	int ret;
	char *buf;
	struct request *req = dat;

	if (!req->connected && libe_fd_evs(fd) & LIBE_WR) {
		daikin_request_send(fd, req);
		libe_mod_fd(fd, LIBE_RD);
		return;
	}
	ret = recv(fd, rbuf+rfill, sizeof(rbuf)-1-rfill, MSG_DONTWAIT);
	if (ret < 0 && errno == EAGAIN)
		return;
	if (ret < 0)
		mylog(LOG_ERR, "recv %s: %s", daikin_host, ESTR(errno));

	rfill += ret;
	if (!ret || (rfill >= sizeof(rbuf)-1)) {
		int req_control_info;
		int req_ok = 0;
		/* parse */
		rbuf[rfill] = 0;
		buf = strstr(rbuf, "\r\n\r\n");
		if (buf) {
			buf += 4;
			req_ok = !strncmp("ret=OK", buf, 6);
			if (!req_ok)
				mylog(LOG_WARNING | LOG_MQTT, "request to %s failed: '%s'",
						daikin_host, req->str);
			daikin_recvd_payload(buf, (rbuf+rfill)-buf);
			rfill = 0;
		}

		close(fd);
		libe_remove_fd(fd);
		struct request *next = req->next;
		/* consume request from q */
		reqq = next;
		if (reqqend == req)
			reqqend = NULL;
		req_control_info = !strcmp(req->str ?: "", "/aircon/get_control_info");
		free(req);
		if (next)
			daikin_request_start(next);

		if (req_ok && req_control_info && !dev_ready) {
			/* initial control_info successfully received */
			dev_ready = 1;
			mylog(LOG_NOTICE, "initial remote data recvd");
			if (mqtt_ready) {
				initial_merge();
				if (dirty)
					update();
			}
		}
	}
}

/* mqtt iface */
static char *topicfmt_str;
__attribute__((format(printf,1,2)))
static const char *topicfmt(const char *fmt, ...)
{
	va_list va;

	xfree(topicfmt_str);
	va_start(va, fmt);
	vasprintf(&topicfmt_str, fmt, va);
	va_end(va);
	return topicfmt_str;
}
static char *payloadfmt_str;
__attribute__((unused))
__attribute__((format(printf,1,2)))
static const char *payloadfmt(const char *fmt, ...)
{
	va_list va;

	xfree(payloadfmt_str);
	va_start(va, fmt);
	vasprintf(&payloadfmt_str, fmt, va);
	va_end(va);
	return payloadfmt_str;
}

static void mqtt_pub(const char *topic, const char *payload, int retain, char **cached)
{
	int ret;

	if (cached && retain) {
		if (!strcmp(payload ?: "", *cached ?: ""))
			/* equal */
			return;
	}
	mylog(LOG_DEBUG, "mqtt:>%s %s", topic, payload ?: "<null>");
	ret = mosquitto_publish(mosq, NULL, topic, strlen(payload?:""), payload, mqtt_qos, retain);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
	if (cached && retain) {
		xfree(*cached);
		*cached = strdup(payload);
	}
}

static void mqtt_sub(const char *topic)
{
	int ret;

	ret = mosquitto_subscribe(mosq, NULL, topic, mqtt_qos);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", topic, mosquitto_strerror(ret));
}

static void mqttlog(int loglevel, const char *msg)
{
	static const char *const prionames[] = {
		[LOG_EMERG] = "emerg",
		[LOG_ALERT] = "alert",
		[LOG_CRIT] = "crit",
		[LOG_ERR] = "err",
		[LOG_WARNING] = "warn",
		[LOG_NOTICE] = "notice",
		[LOG_INFO] = "info",
		[LOG_DEBUG] = "debug",
	};

	mqtt_pub(topicfmt("log/%s/%s", NAME, prionames[loglevel & LOG_PRIMASK]), msg, 0, NULL);
}

static void my_mqtt_log(struct mosquitto *mosq, void *userdata, int level, const char *str)
{
	static const int logpri_map[] = {
		MOSQ_LOG_ERR, LOG_ERR,
		MOSQ_LOG_WARNING, LOG_WARNING,
		MOSQ_LOG_NOTICE, LOG_NOTICE,
		MOSQ_LOG_INFO, LOG_INFO,
		MOSQ_LOG_DEBUG, LOG_DEBUG,
		0,
	};
	int j;

	for (j = 0; logpri_map[j]; j += 2) {
		if (level & logpri_map[j]) {
			//mylog(logpri_map[j+1], "[mosquitto] %s", str);
			return;
		}
	}
}

static int test_set_topic(const char *topic, const char *main, int retain)
{
	if (retain)
		return !strcmp(topic, main);
	int len = strlen(main);

	return strlen(topic) == len+4
		&& !strncmp(topic, main, len)
		&& !strcmp(topic+len, "/set");
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	char *topic, *payload;

	if (is_self_sync(msg)) {
		++mqtt_ready;
		if (mqtt_ready == 1 && dev_ready) {
			initial_merge();
			if (dirty)
				/* this is unlikely to happen,
				 * since we clear state on exit
				 */
				update();
		}
		return;
	}

	mylog(LOG_DEBUG, "mqtt:<%s %s", msg->topic, (char *)msg->payload ?: "<null>");

	if (!strncmp(work_topic, msg->topic, work_topiclen)) {
		topic = msg->topic + work_topiclen;
		payload = (char *)msg->payload;

		if (msg->retain && !strcmp(topic, "/pow")) {
			setp_mqtt.pow = strtoul(payload, NULL, 0);

		} else if (msg->retain && !strcmp(topic, "/mode")) {
			setp_mqtt.mode = strtomode(payload);

		} else if (msg->retain && !strcmp(topic, "/stemp")) {
			setp_mqtt.temp = strtod_safe(payload);

		} else if (msg->retain && !strcmp(topic, "/fan")) {
			setp_mqtt.fan = strtofan(payload);

		} else if (msg->retain && !strcmp(topic, "/wing")) {
			setp_mqtt.wing = strtowing(payload);

		} else if (test_set_topic(topic, "/pow", msg->retain)) {
pow_requested:
			setp.pow = strtoul(payload, NULL, 0);
			update();

		} else if (test_set_topic(topic, "/mode", msg->retain)) {
mode_requested:;
			int new_mode = strtomode(payload);

			if (new_mode < 0) {
				mylog(LOG_WARNING, "unknown mode '%s' requested", payload);
				return;
			}
			if (new_mode != setp.mode) {
				setp.temp = modecfgs[new_mode].temp;
				setp.fan = modecfgs[new_mode].fan;
				setp.mode = new_mode;
			}
			update();

		} else if (test_set_topic(topic, "/homekit/setstate", msg->retain)) {
			int hkmode  = strtoul(payload, NULL, 0);

			switch (hkmode) {
			case HKAIRCO:
				if (hkmode < 0 || hkmode >= NHKSETSTATES)
					return;
				payload = (char *)hksetstates[hkmode];
				goto mode_requested;
				break;
			case HKTHERMOSTAT:
				if (hkmode < 0 || hkmode >= NHKTHRMSTATES)
					return;
				if (!hkmode)
					goto pow_requested;
				payload = (char *)hkthrmstates[hkmode];
				goto mode_requested;
				break;
			}

		} else if (test_set_topic(topic, "/stemp", msg->retain)) {
			setp.temp = strtod_safe(payload);
			update();

		} else if (test_set_topic(topic, "/fan", msg->retain)) {
			setp.fan = strtofan(payload);
			update();

		} else if (test_set_topic(topic, "/wing", msg->retain)) {
			setp.wing = strtowing(payload);
			update();

		} else if (test_set_topic(topic, "/cfg/loglevel", msg->retain)) {
			if (!msg->payloadlen)
				return;
			set_loglevel(strtoul(payload, NULL, 0));
			mqtt_pub(topicfmt("%s/cfg/loglevel", work_topic), payloadfmt("%i", max_loglevel), 1, NULL);

		} else if (test_set_topic(topic, "/cfg/dryrun", msg->retain)) {
			if (!msg->payloadlen)
				return;
			dryrun = strtoul(payload, NULL, 0);
			mqtt_pub(topicfmt("%s/cfg/dryrun", work_topic), payloadfmt("%i", dryrun), 1, NULL);
		}
	}
}

static void mqtt_maintenance(void *dat)
{
	int ret;
	struct mosquitto *mosq = dat;

	ret = mosquitto_loop_misc(mosq);
	if (ret)
		mylog(LOG_ERR, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
	libt_add_timeout(2.3, mqtt_maintenance, dat);
}

static void recvd_mosq(int fd, void *dat)
{
	struct mosquitto *mosq = dat;
	int evs = libe_fd_evs(fd);
	int ret;

	if (evs & LIBE_RD) {
		/* mqtt read ... */
		ret = mosquitto_loop_read(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
	}
	if (evs & LIBE_WR) {
		/* flush mqtt write queue _after_ the timers have run */
		ret = mosquitto_loop_write(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
	}
}

void mosq_update_flags(void)
{
	if (mosq)
		libe_mod_fd(mosquitto_socket(mosq), LIBE_RD | (mosquitto_want_write(mosq) ? LIBE_WR : 0));
}

int main(int argc, char *argv[])
{
	int opt, ret;
	char *str;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'm':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'h':
		daikin_host = optarg;
		break;
	case 'n':
		dryrun = 1;
		break;

	case 'w':
		work_topic = optarg;
		break;
	case 'k':
		if (!strncasecmp(optarg, "thermo", 5))
			homekit = HKTHERMOSTAT;
		else if (!strcasecmp(optarg, "heatercooler"))
			homekit = HKAIRCO;
		break;
	}

	if (!daikin_host)
		mylog(LOG_ERR, "no daikin host defined");

	/* Daikin setup */
	daikin_resolve(daikin_host);
	libt_add_timeout(0, request_info, NULL);
	libt_add_timeout(0, request_state, NULL);

	/* MQTT start */
	work_topiclen = strlen(work_topic);
	mosquitto_lib_init();
	mosq = mosquitto_new(csprintf(NAME "-%i", getpid()), true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_log_callback_set(mosq, my_mqtt_log);
	mosquitto_message_callback_set(mosq, my_mqtt_msg);
	mosquitto_will_set(mosq, topicfmt("%s/state", work_topic), 4, "lost", mqtt_qos, 1);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive ?: 60);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* subscribe to topics */
	mqtt_sub(topicfmt("%s/#", work_topic));

	libt_add_timeout(0, mqtt_maintenance, mosq);
	libe_add_fd(mosquitto_socket(mosq), recvd_mosq, mosq);

	/* prepare signalfd */
	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);
	/* for inside GDB */
	//sigdelset(&sigmask, SIGINT);

	if (sigprocmask(SIG_BLOCK, &sigmask, NULL) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, signalrecvd, NULL);

	send_self_sync(mosq, mqtt_qos);
	mqtt_pub(topicfmt("%s/state", work_topic), "...", 1, &saved_state);
	/* core loop */
	for (; !sigterm;) {
		libt_flush();
		mosq_update_flags();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}
	mylog(LOG_NOTICE, "terminating ...");

	mqtt_pub(topicfmt("%s/state", work_topic), "exit", 1, &saved_state);
	mqtt_pub(topicfmt("%s/pow", work_topic), "", 1, &saved_pow);
	mqtt_pub(topicfmt("%s/mode", work_topic), "", 1, &saved_mode);
	mqtt_pub(topicfmt("%s/stemp", work_topic), "", 1, &saved_stemp);
	mqtt_pub(topicfmt("%s/fan", work_topic), "", 1, &saved_fan);
	mqtt_pub(topicfmt("%s/wing", work_topic), "", 1, &saved_wing);
	mqtt_pub(topicfmt("%s/itemp", work_topic), "", 1, &saved_itemp);
	mqtt_pub(topicfmt("%s/ihum", work_topic), "", 1, &saved_ihum);
	mqtt_pub(topicfmt("%s/otemp", work_topic), "", 1, &saved_otemp);
	mqtt_pub(topicfmt("%s/homekit/curstate", work_topic), "", 1, &saved_hkcurstate);
	mqtt_pub(topicfmt("%s/homekit/setstate", work_topic), "", 1, &saved_hksetstate);

	/* terminate */
	send_self_sync(mosq, mqtt_qos);
	for (; mqtt_ready < 2; ) {
		libt_flush();
		mosq_update_flags();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}
#if 1
	xfree(payloadfmt_str);
	xfree(topicfmt_str);
	xfree(csprintf_str);

	/* cleanup */
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return 0;
}
