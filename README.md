# daikinmqtt

daikinmqtt is a bridge between daikin Airconditioners and MQTT

# example use

Run these commands (or start with your init system).

	$ daikinmqtt -h HOST_OR_IP -w MQTT_PREFIX/ -v &

This will export the airconditioner properties into MQTT,
under MQTT_PREFIX/

controlling is possible by publishing to MQTT_PREFIX/PROP/set (non-retained).

